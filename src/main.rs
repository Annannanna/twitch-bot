//! A little question-and-answer bot for twitch chat & an excuse to test stream
//!
//! No cool features, just !command → raw text

use async_std::fs::File;
use async_std::io::BufReader;
use async_std::path::PathBuf;
use async_std::prelude::*;
use async_std::sync::{Arc, RwLock};
use chrono::{FixedOffset, TimeZone};
use notify::{recommended_watcher, EventKind, RecursiveMode, Watcher};
use std::collections::HashMap;
use std::path::Path;
use twitchchat::connector::async_std::ConnectorTls;
use twitchchat::messages::{Commands, Privmsg};
use twitchchat::twitch::Capability;
use twitchchat::{AsyncRunner, PrivmsgExt, Status, UserConfig, Writer};

// who needs configurability
const CHANNEL_NAME: &str = "reanna__";
const RESPONSE_FILE_NAME: &str = "responses.txt";
const TIMEZONES_FILE_NAME: &str = "timezones.json";

type SerializedTimezones = HashMap<String, i32>;
type Timezones = HashMap<String, FixedOffset>;

fn load_timezones() -> anyhow::Result<Timezones> {
    let mut file = std::fs::File::open(TIMEZONES_FILE_NAME)?;
    let timezones: SerializedTimezones = serde_json::from_reader(&mut file)?;
    Ok(timezones
        .into_iter()
        .map(|(name, offset)| (name, FixedOffset::east(offset)))
        .collect())
}

fn save_timezones(timezones: &Timezones) -> anyhow::Result<()> {
    let mut file = std::fs::File::create(TIMEZONES_FILE_NAME)?;
    let serialized: SerializedTimezones = timezones
        .iter()
        .map(|(name, offset)| (name.clone(), offset.local_minus_utc()))
        .collect();
    serde_json::to_writer_pretty(&mut file, &serialized)?;
    Ok(())
}

struct Responses {
    filename: PathBuf,
    data: HashMap<String, String>,
}

impl Responses {
    fn new(filename: PathBuf) -> Self {
        Self {
            filename,
            data: Default::default(),
        }
    }

    async fn load(filename: PathBuf) -> anyhow::Result<Self> {
        let mut responses = Self::new(filename);
        responses.reload().await?;
        Ok(responses)
    }

    async fn reload(&mut self) -> anyhow::Result<()> {
        let mut responses = HashMap::new();

        let f = BufReader::new(File::open(&self.filename).await?);
        let mut lines = f.lines();
        while let Some(line) = lines.next().await {
            let line = line?;
            if line.trim().is_empty() || line.starts_with('#') {
                continue;
            }

            let pos = if let Some(pos) = line.find(' ') {
                pos
            } else {
                anyhow::bail!(
                    "{} format is incorrect, should be !cmd<space>response",
                    RESPONSE_FILE_NAME
                );
            };
            let (command, response) = line.split_at(pos);

            responses.insert(command.to_string(), (&response[1..]).to_string());
        }

        self.data = responses;
        Ok(())
    }

    fn get(&self, cmd: &str) -> Option<&str> {
        self.data.get(cmd).map(|s| s.as_str())
    }
}

async fn handle_message(mut writer: Writer, responses: &Responses, incoming: Privmsg<'_>) {
    let mut parts = incoming.data().split_whitespace();

    match parts.nth(0) {
        Some(cmd) => {
            if let Some(response) = responses.get(cmd) {
                writer.reply(&incoming, response).unwrap();
            }
        }
        None => return,
    }
}

#[async_std::main]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv()?;

    let username = dotenv::var("TWITCH_USERNAME")?;
    let token = dotenv::var("TWITCH_TOKEN")?;

    let user_config = UserConfig::builder()
        .name(username)
        .token(token)
        .capabilities(&[Capability::Tags])
        .build()?;
    let connector = ConnectorTls::twitch()?;

    let responses = Arc::new(RwLock::new(
        Responses::load(RESPONSE_FILE_NAME.parse()?).await?,
    ));
    let (tx, rx) = std::sync::mpsc::channel();
    let mut watcher = recommended_watcher(move |res| match res {
        Ok(event) => tx.send(event).unwrap(),
        Err(_) => (),
    })?;
    watcher.watch(Path::new("."), RecursiveMode::NonRecursive)?;

    // maybe should just be a proper thread since recv() blocks
    let update_responses = Arc::clone(&responses);
    async_std::task::spawn(async move {
        loop {
            // Could be worth unblock()-ing this so it runs on a separate thread
            // and doesn't clog up the task queue
            let event = rx.recv().unwrap();
            match event.kind {
                EventKind::Create(_)
                | EventKind::Modify(_)
                | EventKind::Remove(_)
                    if event.paths.iter().any(|path| path.ends_with(RESPONSE_FILE_NAME)) =>
                {
                    // It's a match, so we reload the responses file
                }
                // Ignore unrelated events
                _ => continue,
            }

            let mut responses = update_responses.write().await;
            match responses.reload().await {
                Ok(_) => (),
                Err(err) => {
                    // Very fancy error recovery
                    dbg!(err);
                    continue;
                }
            }

            println!("reloaded responses");
        }
    });

    let mut runner = AsyncRunner::connect(connector, &user_config).await?;
    runner.join(CHANNEL_NAME).await?;

    let timezones = load_timezones().unwrap_or_default();
    loop {
        match runner.next_message().await {
            Err(err) => {
                dbg!(err);
            }
            Ok(Status::Quit) | Ok(Status::Eof) => break,
            Ok(Status::Message(Commands::Ready(_))) => {
                println!("all joined; waiting for commands");
            }
            Ok(Status::Message(Commands::Privmsg(message))) => {
                let responses = responses.read().await;
                handle_message(runner.writer(), &responses, message).await;
            }
            Ok(Status::Message(_commands)) => continue,
        }
    }

    Ok(())
}
